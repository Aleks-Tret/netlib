#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

using namespace ntlb;

namespace unit_test
{
	TEST_CLASS(netaddress)
	{
	public:
		TEST_METHOD(constructor_1)
		{
			ntlb::wsa_init wsa;
				
			net_addr myAddr("localhost", "80", net_addr::init_addrinfo());
						
			auto result = myAddr.get_nameinfo();

			std::string out = "TEST_METHOD netaddress.constructor_1 said - ";
			out += result.first + " " + result.second;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(constructor_2)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr("", "80", net_addr::init_addrinfo());

			auto result = myAddr.get_nameinfo();

			std::string out = "TEST_METHOD netaddress.constructor_2 said - ";
			out += result.first + " " + result.second;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(constructor_exception_1)
		{
			auto functor = []() {
				net_addr myAddr("localhost", "80", net_addr::init_addrinfo());
			};
			Assert::ExpectException<ntlb::socket_error>(functor);
			//without ntlb::wsa_init wsa;
		}
		TEST_METHOD(constructor_3)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr("localhost", "80", net_addr::init_addrinfo(SOCK_DGRAM, IPPROTO_UDP));

			auto result = myAddr.get_nameinfo();

			std::string out = "TEST_METHOD netaddress.constructor_3 said - ";
			out += result.first + " " + result.second;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(constructor_4)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr("", "", net_addr::init_addrinfo());

			auto result = myAddr.get_nameinfo();

			std::string out = "TEST_METHOD netaddress.constructor_4 said - ";
			out += result.first + " " + result.second;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(constructor_5)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr("", "");

			auto result = myAddr.get_nameinfo();

			std::string out = "TEST_METHOD netaddress.constructor_5 said - ";
			out += result.first + " " + result.second;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(constructor_6)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr;

			auto result = myAddr.get_nameinfo();

			std::string out = "TEST_METHOD netaddress.constructor_6 said - ";
			out += result.first + " " + result.second;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(constructor_exception_2)
		{
			ntlb::wsa_init wsa;

			auto functor = []() {
				net_addr myAddr("not_exist_this_name.host", "100", net_addr::init_addrinfo());
			};
			Assert::ExpectException<ntlb::socket_error>(functor);
		}
		TEST_METHOD(constructor_exception_3)
		{
			ntlb::wsa_init wsa;

			auto functor = []() {
				net_addr myAddr("localhost", "80", net_addr::init_addrinfo(SOCK_STREAM, IPPROTO_UDP, AF_NETBIOS));
			};
			Assert::ExpectException<ntlb::socket_error>(functor);
		}
		TEST_METHOD(get_nameinfo_1)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr("localhost", "80", net_addr::init_addrinfo());
			auto result = myAddr.get_nameinfo(NI_NUMERICHOST | NI_NUMERICSERV);

			Assert::AreEqual("127.0.0.1", result.first.c_str());
			Assert::AreEqual("80", result.second.c_str());
		}
		TEST_METHOD(get_nameinfo_2)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr("yandex.ru", "80", net_addr::init_addrinfo());
			auto result = myAddr.get_nameinfo(NI_NUMERICHOST | NI_NUMERICSERV);

			std::string out = "TEST_METHOD netaddress.get_nameinfo_2 said - ";
			out += "yandex.ru:80 ip address - " + result.first + ":" + result.second;

			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(get_nameinfo_3)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr("127.0.0.1", "80", net_addr::init_addrinfo());

			auto result = myAddr.get_nameinfo(NI_NAMEREQD);
			
			std::string out;
			out = "TEST_METHOD netaddress.get_nameinfo_3 said - ";
			out += result.first + " " + result.second;
			Logger::WriteMessage(out.c_str());
		} 

		TEST_METHOD(get_nameinfo_4)
		{
			ntlb::wsa_init wsa;

			net_addr firstAddr;
			net_addr secondAddr(std::move(firstAddr));

			auto result = firstAddr.get_nameinfo();

			Assert::AreEqual("", result.first.c_str());
			Assert::AreEqual("", result.second.c_str());
		}
		TEST_METHOD(get_addrinfo_1)
		{
			ntlb::wsa_init wsa;

			net_addr myAddr("yandex.ru", "80", net_addr::init_addrinfo());
			auto info = myAddr.get_addrinfo();

			Assert::AreEqual(0, info->ai_flags); //?
			Assert::AreEqual(AF_INET, info->ai_family);
			Assert::AreEqual(SOCK_STREAM, info->ai_socktype);
			Assert::AreEqual(static_cast<int>(IPPROTO_TCP), info->ai_protocol);
			Assert::AreEqual(nullptr, info->ai_canonname);
			Assert::IsFalse(nullptr == info->ai_next);	
		}
	};
}