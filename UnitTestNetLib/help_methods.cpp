#include "stdafx.h"

std::string hlp::process_exception(std::exception_ptr & eptr)
{
	std::string out;
	try {
		std::rethrow_exception(eptr);
	}
	catch (const std::ios_base::failure& e) {
		out += "I/O EXCEPTION " + print_code_exception(e);
	}
	catch (const std::system_error& e) {
		out += "SYSTEM EXCEPTION " + print_code_exception(e);
	}
	catch (const std::future_error& e) {
		out += "FUTURE EXCEPTION: " + print_code_exception(e);
	}
	catch (const std::bad_alloc& e) {
		out += "BAD ALLOC EXCEPTION: " + std::string(e.what());
	}
	catch (const std::runtime_error& e) {
		out += "RUNTIME_ERROR: " + std::string(e.what());
	}
	catch (const std::exception& e) {
		out += "EXCEPTION: " + std::string(e.what());
	}
	catch (...) {
		out += "UNKNOWN EXCEPTION ";
	}
	return out;
}
