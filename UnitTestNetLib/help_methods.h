#pragma once
#ifndef HELP_METHODS
#define	HELP_METHODS

namespace hlp
{
	template <typename T>
	std::string print_code_exception(const T& e)
	{
		std::stringstream out;
		auto c = e.code();

		out << "-category: " << c.category().name() << std::endl;
		out << "-value: " << c.value() << std::endl;
		out << "-msg: " << c.message() << std::endl;

		return out.str();
	}

	std::string process_exception(std::exception_ptr& eptr);
}

#endif