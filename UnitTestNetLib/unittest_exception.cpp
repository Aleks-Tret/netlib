#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace unit_test
{
	TEST_CLASS(exception)
	{
	public:
		TEST_METHOD(exception_constructor_1)
		{
			ntlb::socket_error myError(100, "error message");
			Assert::AreEqual(100, myError.error_code());

			std::string message = "TEST_METHOD exception.exception_constructor_1 said - ";
			message +=	myError.what() + std::string(" with code ") + std::to_string(myError.error_code());
			Logger::WriteMessage(message.c_str());
		}
		TEST_METHOD(throw_exception_1)
		{
			auto functor = []() {
				throw ntlb::socket_error(100);
			};
			Assert::ExpectException<std::runtime_error>(functor);
		}
		TEST_METHOD(exception_constructor_2)
		{
			ntlb::timeout_error myError("time is up");

			std::string message = "TEST_METHOD exception.exception_constructor_2 said - ";
			message += myError.what();
			Logger::WriteMessage(message.c_str());
		}
		TEST_METHOD(throw_exception_2)
		{
			auto functor = []() {
				throw ntlb::timeout_error("time is up");
			};
			Assert::ExpectException<std::runtime_error>(functor);
		}
	};
}