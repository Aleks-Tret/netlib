#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace ntlb;

namespace unit_test
{
	TEST_CLASS(definition)
	{
		TEST_METHOD(wsa_init_constructor)
		{
			auto functor = []() {
				ntlb::wsa_init wsa;
			};
			bool isThrown{ false };
			try {
				functor();
			}
			catch (ntlb::socket_error e) {
				std::string message = e.what() + std::string(" with code ")
					+ std::to_string(e.error_code());
				Logger::WriteMessage(message.c_str());
				isThrown = true;
			}
			Assert::IsFalse(isThrown);
		}
		TEST_METHOD(wsa_int_constructor)
		{
			ntlb::wsa_init wsa1;
			ntlb::wsa_init wsa2;
			wsa1.terminate();
			wsa2.terminate();
		}
	};
}