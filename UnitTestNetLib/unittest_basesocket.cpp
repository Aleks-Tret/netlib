#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

using namespace ntlb;

namespace unit_test
{
	TEST_CLASS(basesocket)
	{
	private:
		std::pair<std::string, std::string> connect_to = { "yandex.ru", "80" };
	public:
		TEST_METHOD(constructor_1)
		{
			base_socket mySocket;
			auto functor = [&mySocket]() {
				mySocket.open();
			};
			Assert::ExpectException<ntlb::socket_error>(functor);
		}
		TEST_METHOD(constructor_2)
		{
			base_socket mySocket;
			std::string out = "TEST_METHOD basesocket.constructor_2 said - ";
			try {
				mySocket.open();
			}
			catch (...) {
				out += hlp::process_exception(std::current_exception());
			}
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(operator_bool_1)
		{
			base_socket mySocket;
			Assert::IsFalse(mySocket);
			Assert::IsTrue(!mySocket);
		}
		TEST_METHOD(operator_bool_2)
		{
			wsa_init wsa;

			base_socket mySocket;
			mySocket.open();
			Assert::IsTrue(mySocket);
		}
		TEST_METHOD(construnctor_3)
		{
			wsa_init wsa;

			base_socket mySocket(SOCK_STREAM, IPPROTO_TCP, AF_INET);
			Assert::IsTrue(mySocket);
		}
		TEST_METHOD(connect_1)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();

			auto functor = [&mySocket]() {
				mySocket.connect("12345", "abcd");
			};
			Assert::ExpectException<ntlb::socket_error>(functor);
		}
		TEST_METHOD(move_constructor_1)
		{
			wsa_init wsa;
			base_socket first;
			first.open();

			base_socket second(std::move(first));
			Assert::IsFalse(first);
			Assert::IsTrue(second);
		}
		TEST_METHOD(connect_2)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			bool is_thrown{ false };
			try {
				mySocket.connect(connect_to.first, connect_to.second);
			}
			catch (...) {
				is_thrown = true;
			}
			Assert::IsFalse(is_thrown);
		}
		TEST_METHOD(connect_3)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			ntlb::net_addr addr(connect_to.first, connect_to.second, ntlb::net_addr::init_addrinfo());
			mySocket.connect(addr);
		}
		TEST_METHOD(connect_4)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			ntlb::net_addr addr(connect_to.first, connect_to.second);
			mySocket.connect(addr);
		}
		TEST_METHOD(connect_5)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			mySocket.set_timeout(0.1);
			ntlb::net_addr addr(connect_to.first, connect_to.second);
			mySocket.connect(addr);
		}
		TEST_METHOD(send_and_recv_1)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			mySocket.set_timeout(0.1);
			ntlb::net_addr addr(connect_to.first, connect_to.second);
			mySocket.connect(addr);
			std::stringstream message;
			message << "GET / HTTP/1.1" << "\r\n" 
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n";
			mySocket.send(message.str());

			std::string request, buffer;
			int count{};
			while (mySocket.recv(buffer, 100)) {
				request += buffer;
				count++;
			}
			

			std::string out;
			out = "TEST_METHOD basesocket.send_and_recv_1 said - ";
			out += "i called function recv() - " + std::to_string(count) + " times";
			out += "\n" + request;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(set_option_1)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			bool is_thrown{ false };
			try {
				mySocket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);
			}
			catch (...) {
				is_thrown = true;
			}
			Assert::IsFalse(is_thrown);
		}
		TEST_METHOD(get_option_1)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();

			mySocket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);

			auto opt = mySocket.get_option<char>(SOL_SOCKET, SO_REUSEADDR);
			Assert::IsTrue(char(1) == opt);
		}
		TEST_METHOD(bind_1)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			mySocket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);
			ntlb::net_addr myAddr("", "10500");
			bool is_thrown = false;
			try {
				mySocket.bind(myAddr);
			}
			catch (std::exception& e) {
				is_thrown = true;
				std::string out;
				out = "TEST_METHOD basesocket.bind_1 said - ";
				out += e.what();
				Logger::WriteMessage(out.c_str());
			}
			Assert::IsFalse(is_thrown);
		}
		TEST_METHOD(bind_2)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			mySocket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);

			bool is_thrown = false;
			try {
				mySocket.bind("10500");
			}
			catch (std::exception& e) {
				is_thrown = true;
				std::string out;
				out = "TEST_METHOD basesocket.bind_2 said - ";
				out += e.what();
				Logger::WriteMessage(out.c_str());
			}
			Assert::IsFalse(is_thrown);
		}
		TEST_METHOD(bind_3)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			mySocket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);

			bool is_thrown = false;
			try {
				mySocket.bind(10500);
			}
			catch (std::exception& e) {
				is_thrown = true;
				std::string out;
				out = "TEST_METHOD basesocket.bind_1 said - ";
				out += e.what();
				Logger::WriteMessage(out.c_str());
			}
			Assert::IsFalse(is_thrown);
		}
		TEST_METHOD(get_1)
		{
			wsa_init wsa;
			base_socket first;
			first.open();

			Assert::AreNotEqual(-1, first.get());
			Assert::IsTrue(int(first) != -1);

			base_socket second;

			Assert::AreEqual(-1, second.get());
		}
		TEST_METHOD(connect_7)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			ntlb::net_addr addr(connect_to.first, connect_to.second);
			mySocket.connect(addr);
			mySocket.shutdown(ntlb::shut_mode::shut_rd);
			mySocket.close();
			Assert::IsFalse(mySocket.is_valid());
		}
		TEST_METHOD(local_address_1)
		{
			wsa_init wsa;
			base_socket s;
			s.open();

			std::string out;

			Assert::ExpectException<ntlb::socket_error>([&out, &s] {out += ntlb::sockaddr_to_pair(s.local_address()).first; });
		}
		TEST_METHOD(local_address_2)
		{
			wsa_init wsa;
			base_socket s;
			s.open();
			ntlb::net_addr addr(connect_to.first, connect_to.second);
			s.connect(addr);
			
			std::string out;
			out = "TEST_METHOD basesocket.local_address_2 said - ";

			auto result = ntlb::sockaddr_to_pair(s.local_address());
			out += "local address is - " + result.first
				+ " port is - " + result.second;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(remote_address_1)
		{
			wsa_init wsa;
			base_socket s;
			s.open();
			s.connect(ntlb::net_addr(connect_to.first, connect_to.second));

			std::string out;
			out = "TEST_METHOD basesocket.remote_address_1 said - ";

			auto result = ntlb::sockaddr_to_pair(s.remote_address());
			out += "remote address is - " + result.first
				+ " port is - " + result.second;
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(accept_1)
		{
			wsa_init wsa;

			base_socket listen_socket;
			listen_socket.open();
			listen_socket.set_timeout(1.0);
			listen_socket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);
			net_addr addr("localhost", "7500");
			listen_socket.bind(addr);
			listen_socket.listen();

			struct ::sockaddr_in remote { 0 };
			std::string out = "TEST_METHOD basesocket.accept_1 said - ";
			try {
				ntlb::base_socket server_socket = listen_socket.accept(remote);
			}
			catch (...) {
				out += hlp::process_exception(std::current_exception());
			}
			
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(connect_in_thread_1)
		{
			wsa_init wsa;

			auto functor = [&] ()->std::string{
				
				base_socket client_socket;
				client_socket.open();
				client_socket.connect(connect_to.first, connect_to.second);

				std::stringstream message;
				message << "GET / HTTP/1.1" << "\r\n"
					<< "Host: www." << connect_to.first
					<< "\r\n" << "Connection: close" << "\r\n" << "\r\n";

				client_socket.send(message.str());

				message.str("");
				std::string buffer;

				while (client_socket.recv(buffer, 1000)) {
					message << buffer;
				}

				return message.str();
			};
			
			auto result = std::async(functor);

			std::string out = "TEST_METHOD basesocket.connect_in_thread_1 said - ";
			out += result.get();

			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(connect_in_thread_2)
		{
			wsa_init wsa;

			auto functor = []()->std::string {
				base_socket server_socket;
				server_socket.open();
				server_socket.set_timeout(2.0);
				server_socket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);
				server_socket.bind(net_addr("localhost", "7500"));
				server_socket.listen();

				struct ::sockaddr_in remote{0};
				std::string out;
				try {
					base_socket accept_socket = server_socket.accept(remote);
					std::string message = "hellow";
					accept_socket.send(message);
				}
				catch (...) {
					out = hlp::process_exception(std::current_exception());
				}
				return out;
			};
			
			auto result = std::async(std::launch::async, functor);

			std::string out = "TEST_METHOD basesocket.connect_in_thread_2 said - ";
			out += result.get();
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(connect_in_thread_3)
		{
			wsa_init wsa;

			auto functor = []()->std::string {

				base_socket client_socket;
				client_socket.open();
				client_socket.connect("localhost", "7501");

				std::string message;
				client_socket.recv(message, 100);

				return message;
			};

			auto result = std::async(functor);
			
			base_socket server_socket;
			server_socket.open();
			server_socket.set_timeout(2.0);
			server_socket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);
			server_socket.bind(net_addr("localhost", "7501"));
			server_socket.listen();

			struct ::sockaddr_in remote { 0 };
			std::string out = "TEST_METHOD basesocket.connect_in_thread_3 said - ";
			try {
				base_socket accept_socket = server_socket.accept(remote);
				std::string message = "hellow";
				accept_socket.send(message);
			}
			catch (ntlb::socket_error& e) {
				out += e.what();
				out += " " + std::to_string(e.error_code());
			}
			catch (...) {
				out += hlp::process_exception(std::current_exception());
			}
			
			out += result.get();
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(connect_in_thread_4)
		{
			wsa_init wsa;

			auto server = []()->std::pair<std::string, std::string> {
				base_socket server_socket;
				server_socket.open();
				server_socket.set_timeout(3.0);
				server_socket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);
				server_socket.bind(net_addr("localhost", "7501"));
				server_socket.listen();

				struct ::sockaddr_in remote { 0 };
				base_socket accept_socket = server_socket.accept(remote);
				std::string message = "world";
				accept_socket.send(message);

				return ntlb::sockaddr_to_pair(remote);
			};

			auto client = []()->std::string {
				base_socket client_socket;
				client_socket.open();
				client_socket.connect("localhost", "7501");

				std::string message;
				client_socket.recv(message, 100);

				return message;
			};

			auto s = std::async(server);
			auto c = std::async(client);

			auto result = s.get();
			std::string out = "TEST_METHOD basesocket.connect_in_thread_4 said - \n";
			out += "server send message to client with addres - " + result.first
				+ " port - " + result.second + "\n";
			out += "client recv message - " + c.get();
			
			Logger::WriteMessage(out.c_str());
		}
		TEST_METHOD(accept_socket_in_new_thread)
		{
			//�������������� �� ��� ������. ������?
			wsa_init wsa;
			
			std::deque<ntlb::base_socket> accept_sockets;
			std::mutex deque_mutex;
			std::condition_variable cv_queue;
			std::atomic<bool> is_stop_loop(false);

			auto main_loop = [&accept_sockets, &deque_mutex, &is_stop_loop,	&cv_queue]() {

				auto client_functor = [](ntlb::base_socket&& client_socket) {
					std::string message = "hellow";
					try {
						client_socket.send(message);
					}
					catch (...) {
						return;
					}
				};
				
				while (!is_stop_loop.load()) {
					base_socket accept_socket;
					{
						std::unique_lock<std::mutex> ul(deque_mutex);
						cv_queue.wait(ul, [&accept_sockets]() {return !accept_sockets.empty(); });

						if (!accept_sockets.empty()){
							accept_socket = std::move(accept_sockets.front());
							accept_sockets.pop_front();
						}
					}

					std::thread client(client_functor, std::move(accept_socket));
					client.detach();
				}
			};
					
			auto server = [&accept_sockets, &deque_mutex, &is_stop_loop, &cv_queue]() {

				base_socket server_socket;
				server_socket.open();
				server_socket.set_timeout(3.0);
				server_socket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);
				server_socket.bind(net_addr("localhost", "7501"));
				server_socket.listen();
				
				while (!is_stop_loop.load()) {
					struct ::sockaddr_in remote { 0 };
					try {
						base_socket accept_socket = server_socket.accept(remote);
						{
							std::lock_guard<std::mutex> lg(deque_mutex);
							accept_sockets.push_back(std::move(accept_socket));
						}
						cv_queue.notify_one();
					}
					catch (ntlb::socket_error) {
						continue;
					}
					catch (ntlb::timeout_error) {
						continue;
					}				
				}
			};

			auto client = []()->std::string {
				base_socket client_socket;
				client_socket.open();
				client_socket.connect("localhost", "7501");

				std::string message;
				client_socket.recv(message, 100);

				return message;
			};

			std::thread main_loop_thread(main_loop);
			main_loop_thread.detach();

			std::thread server_thread(server);
			server_thread.detach();
			
			auto c1 = std::async(client);
			auto c2 = std::async(client);
			
			auto result_1 = c1.get();
			auto result_2 = c2.get();
			Assert::AreEqual("hellow", result_1.c_str());
			Assert::AreEqual("hellow", result_2.c_str());

			is_stop_loop.store(true);
		}
	};
}