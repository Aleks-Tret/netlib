#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace ntlb;

namespace unit_test 
{
	TEST_CLASS(server)
	{
	private:
		std::pair<std::string, std::string> connect_to = { "localhost", "75011" };

		std::deque<ntlb::base_socket> accept_sockets;
		std::mutex deque_mutex;
		std::condition_variable cv_queue;
		std::atomic<bool> is_stop_loop;
	public:
		server() {

			is_stop_loop.store(false);

			auto main_loop = [&]() {
				wsa_init wsa;
				auto client_functor = [](ntlb::base_socket&& client_socket) {
					std::string message = "hellow";
					try {
						client_socket.send(message);
					}
					catch (...) {
						return;
					}
				};

				base_socket ac_socket;
				std::unique_lock<std::mutex> ul(deque_mutex, std::defer_lock);

				while (!is_stop_loop.load()) {
					{
						std::this_thread::sleep_for(std::chrono::milliseconds(100));
						ul.lock();
						cv_queue.wait(ul, [&]() {return !accept_sockets.empty(); });
						if (!accept_sockets.empty()) {
							ac_socket = std::move(accept_sockets.front());
							accept_sockets.pop_front();

							ul.unlock();

							std::thread client(client_functor, std::move(ac_socket));
							client.detach();
						}
						else {
							ul.unlock();
						}
					}
				}
			};

			auto server = [&]() {
				wsa_init wsa;

				base_socket server_socket;
				try {
					server_socket.open();
					server_socket.set_timeout(3.0);
					server_socket.set_option(SOL_SOCKET, SO_REUSEADDR, 1);
					server_socket.bind(net_addr(connect_to.first, connect_to.second));
					server_socket.listen();
				}
				catch (ntlb::socket_error& e) {
					std::string err = std::string("server said - ") + e.what() + std::string(" ") + std::to_string(e.error_code());
					Logger::WriteMessage(err.c_str());
					return;
				}

				struct ::sockaddr_in remote { 0 };
				while (!is_stop_loop.load()) {
					try {
						base_socket accept_socket = server_socket.accept(remote);
						{
							std::lock_guard<std::mutex> lg(deque_mutex);
							accept_sockets.push_back(std::move(accept_socket));
						}
						cv_queue.notify_one();
					}
					catch (ntlb::socket_error& e) {
						std::string err = e.what() + std::string(" ") + std::to_string(e.error_code());
						Logger::WriteMessage(err.c_str());
						return;
					}
					catch (ntlb::timeout_error& e) {
						std::string err = e.what();
						Logger::WriteMessage(err.c_str());
						continue;
					}
				}
			};

			std::thread main_loop_thread(main_loop);
			main_loop_thread.detach();

			std::thread server_thread(server);
			server_thread.detach();
		}

		~server() {
			is_stop_loop.store(true);
		}

		TEST_METHOD(connect_1)
		{
			wsa_init wsa;
			base_socket mySocket;
			mySocket.open();
			mySocket.set_timeout(3.0);
			ntlb::net_addr addr(connect_to.first, connect_to.second);
			mySocket.connect(addr);

			std::string message;
			mySocket.recv(message, 100);
			Assert::AreEqual("hellow", message.c_str());
		}
	};
}