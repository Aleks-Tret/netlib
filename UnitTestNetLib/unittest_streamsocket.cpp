#include "stdafx.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

using namespace ntlb;

namespace unit_test
{
	TEST_CLASS(streamsocket)
	{
	private:
		wsa_init wsa;
		std::stringstream get_request;

		std::pair<std::string, std::string> connect_to = { "yandex.ru", "80" };
	
	public:
		streamsocket()
		{
			get_request << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n";
		}
		TEST_METHOD(constructor_1)
		{
			socket_buf my_buf;
			my_buf.open();
			my_buf.connect(ntlb::net_addr(connect_to.first, connect_to.second));

			std::ostream client(&my_buf);
			client << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n"
				<< std::flush;

			std::string message = "TEST_METHOD streamsocket.constructor_1 said - \n";
			std::string buffer;

			while (my_buf.recv(buffer, 1000)) {
				message += buffer;
			}

			Logger::WriteMessage(message.c_str());
		}
		TEST_METHOD(constructor_2)
		{
			base_socket my_socket;
			my_socket.open();
			my_socket.connect(net_addr(connect_to.first, connect_to.second));

			socket_buf my_buf(std::move(my_socket));
			
			std::ostream client(&my_buf);
			client << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n"
				<< std::flush;

			std::string buffer;
			int count{};
			while (my_buf.recv(buffer, 1000))
				count++;

			Assert::IsTrue(0 < count);
		}
		TEST_METHOD(constructor_3)
		{
			socket_buf my_buf(SOCK_STREAM, IPPROTO_TCP, AF_INET);
			my_buf.connect(net_addr(connect_to.first, connect_to.second));

			std::ostream client(&my_buf);
			client << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n"
				<< std::flush;

			std::string buffer;
			int count{};
			while (my_buf.recv(buffer, 1000))
				count++;

			Assert::IsTrue(0 < count);
		}
		TEST_METHOD(constructor_4)
		{
			socket_buf my_buf;
			my_buf.open();
			my_buf.connect(ntlb::net_addr(connect_to.first, connect_to.second));

			std::iostream client(&my_buf);
			client << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n"
				<< std::flush;

			std::string buffer = "TEST_METHOD streamsocket.constructor_4 said - \n";
			std::istreambuf_iterator<char> inIter(client);
			std::istreambuf_iterator<char> endIter;

			std::copy(inIter, endIter, std::back_inserter(buffer));
			Logger::WriteMessage(buffer.c_str());		
		}
		TEST_METHOD(xsputn_1)
		{
			socket_buf my_buf;
			my_buf.open();
			my_buf.connect(ntlb::net_addr(connect_to.first, connect_to.second));

			std::iostream client(&my_buf);

			std::stringstream out;
			out << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n";

			my_buf.sputn(out.str().c_str(), out.str().size());
			//out >> client.rdbuf();

			out.str("TEST_METHOD streamsocket.xsputn_1 said - \n");
			out << client.rdbuf();

			Logger::WriteMessage(out.str().c_str());		
		}
		TEST_METHOD(xsgetn_1)
		{
			socket_buf my_buf;
			my_buf.open();
			my_buf.connect(ntlb::net_addr(connect_to.first, connect_to.second));

			std::iostream client(&my_buf);
			client << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n"
				<< std::flush;

			std::string buffer = "TEST_METHOD streamsocket.xsgetn_1 said - \n";

			const int size = 512;
			char str[size];

			int result{};
			while (result != EOF) {
				buffer.append(str, result);
				result = static_cast<int>(my_buf.sgetn(str, size));
			}

			Logger::WriteMessage(buffer.c_str());	
		}
		TEST_METHOD(sungetc_1)
		{
			socket_buf my_buf;
			my_buf.open();
			my_buf.connect(ntlb::net_addr(connect_to.first, connect_to.second));

			std::stringstream out;
			out << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n";

			my_buf.sputn(out.str().c_str(), out.str().size());

			int first_count{};
			char c;
			while ((c = my_buf.sbumpc()) != EOF) {
				first_count++;
				if (std::div(first_count, 100).rem == 0) {
					my_buf.sputbackc(c);
				}
			}

			my_buf.sputn(out.str().c_str(), out.str().size());

			int second_count{};
			while ((c = my_buf.sbumpc()) != EOF) {
				second_count++;
				if (std::div(second_count, 100).rem == 0) {
					my_buf.sputbackc(c);
				}
			}

			Assert::AreNotEqual(first_count, second_count);
		}
		TEST_METHOD(sungetc_2)
		{
			socket_buf my_buf;
			my_buf.open();
			my_buf.connect(ntlb::net_addr(connect_to.first, connect_to.second));

			std::stringstream out;
			out << "GET / HTTP/1.1" << "\r\n"
				<< "Host: www." << connect_to.first
				<< "\r\n" << "Connection: close" << "\r\n" << "\r\n";

			my_buf.sputn(out.str().c_str(), out.str().size());

			int first_count{};
			char c1;
			c1 = my_buf.sbumpc();
			my_buf.sputbackc(c1);
			
			char c2;
			c2 = my_buf.sbumpc();

			Assert::AreEqual(c1, c2);
		}
		TEST_METHOD(move_constructor_1)
		{
			socket_buf first_buf;
			first_buf.open();
			first_buf.connect(net_addr(connect_to.first, connect_to.second));

			socket_buf second_buf(std::move(first_buf));
			
			std::iostream client(&second_buf);

			second_buf.sputn(get_request.str().c_str(), get_request.str().size());

			std::stringstream out;
			out << "TEST_METHOD streamsocket.move_constructor_1 said - \n"
				<< client.rdbuf();

			Logger::WriteMessage(out.str().c_str());			
		}
		TEST_METHOD(move_constructor_2)
		{
			socket_buf first_buf;
			first_buf.open();
			first_buf.connect(net_addr(connect_to.first, connect_to.second));

			socket_buf second_buf(std::move(first_buf));

			std::iostream client(&first_buf);

			second_buf.sputn(get_request.str().c_str(), get_request.str().size());

			std::stringstream out;
			out << client.rdbuf();

			Assert::AreEqual("", out.str().c_str());
		}
	};
}