#include "stdafx.h"
#include "netlib_netaddress.h"

ntlb::net_addr::net_addr(const std::string & host, const std::string & port, const::addrinfo & hint)
{
	int result = ::getaddrinfo(host.c_str(), port.c_str(), &hint, &address_);
	if (result != 0)
		throw ntlb::socket_error(::WSAGetLastError(), "getaddrinfo function fail");
}

ntlb::net_addr::net_addr(const std::string & host, const std::string & port)
{
	const::addrinfo hint = ntlb::net_addr::init_addrinfo();
	int result = ::getaddrinfo(host.c_str(), port.c_str(), &hint, &address_);
	//int result = ::getaddrinfo(host.c_str(), port.c_str(), nullptr, &address_); //not work this->bind()
	if (result != 0)
		throw ntlb::socket_error(::WSAGetLastError(), "getaddrinfo function fail");
}

ntlb::net_addr::net_addr() : net_addr(std::string(), std::string()){}

ntlb::net_addr::net_addr(net_addr && that) noexcept : address_(that.address_) 
{
	that.address_ = nullptr;
}

ntlb::net_addr & ntlb::net_addr::operator=(net_addr && that) noexcept
{
	if (this == &that)
		return *this;

	if (address_ != nullptr)
		::freeaddrinfo(address_);

	address_ = nullptr;
	std::swap(this->address_, that.address_);
	return *this;
}

ntlb::net_addr::~net_addr()
{
	if (address_ != nullptr)
		::freeaddrinfo(address_);
}

::addrinfo ntlb::net_addr::init_addrinfo(const int socktype, const int protocol, 
	const int family, const int flags) noexcept
{
	struct ::addrinfo info { 0 };

	info.ai_flags = flags;
	info.ai_family = family;
	info.ai_socktype = socktype;
	info.ai_protocol = protocol;

	return info;
}

std::pair<std::string, std::string> ntlb::net_addr::get_nameinfo(const int flags) const
{
	if (address_ == nullptr)
		return std::make_pair(std::string(), std::string());

	char host[NI_MAXHOST];
	char port[NI_MAXSERV];

	int result = ::getnameinfo(address_->ai_addr, address_->ai_addrlen,
		host, NI_MAXHOST, port, NI_MAXSERV, flags);
	if (result != 0)
		throw ntlb::socket_error(WSAGetLastError(), "getnameinfo function fail");

	return std::make_pair(std::string(host), std::string(port));
}
