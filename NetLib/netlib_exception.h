#pragma once
#include "stdafx.h"

#ifndef NETLIB_EXCEPTION
#define NETLIB_EXCEPTION

namespace ntlb
{
	//@brief class to throw an exception
	class socket_error : public std::runtime_error {
	public:
		explicit socket_error(const int code, const std::string& message = std::string())
			: runtime_error(message), error_code_(code) {}

		explicit socket_error(const int code, const char* message)
			: socket_error(code, std::string(message)) {}

		int error_code() const noexcept { return error_code_; }

	private:
		int error_code_{};
	};

	//@brief class to throw an exception when time is up
	class timeout_error : public std::runtime_error {
	public:
		explicit timeout_error(const std::string& message)
			: std::runtime_error(message) {}

		explicit timeout_error(const char* message)
			: timeout_error(std::string(message)) {}
	};
}

#endif