#include "stdafx.h"
#include "netlib_streamsocket.h"

int ntlb::socket_buf::flush_() noexcept
{
	const ptrdiff_t num = basic_streambuf::pptr() - basic_streambuf::pbase();

	try {
		base_socket::send(bout_, num); //��� ���������� ��������!
	}
	catch (std::runtime_error) {
		return EOF;
	}

	basic_streambuf::pbump(-num);

	return num;
}

int ntlb::socket_buf::overflow(int c)
{
	if (this->flush_() == EOF) 
		return EOF;

	if (c != EOF) {
		*basic_streambuf::pptr() = c;
		basic_streambuf::pbump(1);
	}

	return c;
}

int ntlb::socket_buf::sync()
{
	return this->flush_();
}

void ntlb::socket_buf::init_() noexcept
{
	basic_streambuf::setp(bout_, bout_ + size_buf);
	basic_streambuf::setg(bin_ + under_sym, bin_ + under_sym, bin_ + under_sym);
}

std::streamsize ntlb::socket_buf::xsputn(const char * str, std::streamsize length)
{
	std::streamsize offset = 0;

	while (length > 0) {
		//available free space in buffer
		ptrdiff_t free_space = (basic_streambuf::epptr() - 1) - basic_streambuf::pptr();
		
		//how much char will copy to buffer
		std::streamsize number = (length > free_space ? free_space : length);

		std::copy(str + offset, (str + offset) + number, bout_);
		
		length -= number;
		offset += number;

		basic_streambuf::pbump(static_cast<int>(number));

		//still have data, need send it
		if (length >= 0 && this->flush_() == EOF) 
			return EOF;
	}
	
	return offset;
}

void ntlb::socket_buf::close()
{
	this->sync();
	base_socket::close();
}

int ntlb::socket_buf::underflow()
{
	//if we have something to read, do it
	if (basic_streambuf::gptr() < basic_streambuf::egptr())
		return traits_type::to_int_type(*basic_streambuf::gptr());
	//else

	int result = this->fill_();
	if (result == EOF)
		return EOF;

	return traits_type::to_int_type(*basic_streambuf::gptr());
}

int ntlb::socket_buf::fill_() noexcept
{
	ptrdiff_t num_putback = basic_streambuf::gptr() - basic_streambuf::eback();
	num_putback = (num_putback > under_sym ? under_sym : num_putback);

	ptrdiff_t offcet = under_sym - num_putback;
	if (num_putback > 0)
		std::memmove(bin_ + offcet, basic_streambuf::gptr() - num_putback, num_putback);

	int result{};
	try {
		result = base_socket::recv(bin_ + under_sym, size_buf - under_sym);
		if (result == 0)
			return EOF;
	}
	catch (std::runtime_error) {
		return EOF;
	}

	basic_streambuf::setg(bin_ + offcet, bin_ + under_sym, bin_ + result + under_sym);

	return result;
}

std::streamsize ntlb::socket_buf::xsgetn(char * str, std::streamsize length)
{
	std::streamsize offset = 0;

	while (length > 0) {
		//available data for reading
		ptrdiff_t data = basic_streambuf::egptr() - basic_streambuf::gptr();

		if (data == 0 && this->fill_() == EOF)
			return (offset == 0 ? EOF : offset);

		data = basic_streambuf::egptr() - basic_streambuf::gptr();

		std::streamsize number = (length > data ? data : length);

		//std::copy(basic_streambuf::gptr(), basic_streambuf::gptr() + number, str + offset);
		std::memcpy(str + offset, basic_streambuf::gptr(), static_cast<size_t>(number));

		length -= number;
		offset += number;

		basic_streambuf::gbump(static_cast<int>(number));
	}

	return offset;
}