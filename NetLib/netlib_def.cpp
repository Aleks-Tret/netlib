#include "stdafx.h"
#include "netlib_def.h"

ntlb::wsa_init::wsa_init()
{
	try {
		this->init();
	}
	catch (ntlb::socket_error) {
		return;
	}	
}

ntlb::wsa_init::~wsa_init()
{
	try {
		this->terminate();
	}
	catch (ntlb::socket_error) {
		return;
	}
}

void ntlb::wsa_init::terminate()
{
	if (::WSACleanup() == SOCKET_ERROR)
		throw ntlb::socket_error(::WSAGetLastError(), "WSACleanup function fail");
}

void ntlb::wsa_init::init()
{
	int result = ::WSAStartup(MAKEWORD(2, 2), &wsaData_);
	if (result != 0)
		throw ntlb::socket_error(result, "WSAStartup function fail");
}

std::pair<std::string, std::string> ntlb::sockaddr_to_pair(::sockaddr_in addr, const int family)
{
	const int size = BUFFER_SIZE_; //magic constant
	char* buffer = new char[size]; //I don't like it
	auto action = ntlb::finally([&buffer] {delete[] buffer; });

	if (::inet_ntop(family, &addr.sin_addr, buffer, size) == nullptr)
		throw ntlb::socket_error(WSAGetLastError(), "inet_ntop function fail");

	unsigned int port = ::ntohs(addr.sin_port);
	
	return std::make_pair(std::string(buffer), std::to_string(::ntohs(addr.sin_port)));
}
