#pragma once
#include "stdafx.h"

#ifndef NETLIB_DEF
#define NETLIB_DEF

#define BUFFER_SIZE_ 512

namespace ntlb
{
	//@brief class to initiate use of the winsock dll in the process
	//for os windows
	//Need to link with Ws2_32.lib
	//#pragma comment(lib, "ws2_32.lib")
	class wsa_init {
	public:
		wsa_init();
		~wsa_init();

		wsa_init& operator=(const wsa_init&) = delete;
		wsa_init(const wsa_init&) = delete;


		::WSADATA wsadata() const noexcept { return wsaData_; }
		void terminate();
		void init();
	private:
		::WSADATA wsaData_;
	};

	//@brief flag that describes what types of operation will no longer be allowed
	enum class shut_mode {
		 shut_rd = SD_RECEIVE
		,shut_wr = SD_SEND
		,shut_rdwr = SD_BOTH
	};

	//@brief flags that specify the way in which the call send() is made
	enum class msg_mode {
		 msg_non = 0
		,msg_oob = MSG_OOB //for send
		,msg_dontroute = MSG_DONTROUTE //for send
		,msg_peek = MSG_PEEK  //for recv
		,msg_waitall = MSG_WAITALL //for recv
	};

	template <typename F> 
	struct final_action
	{
		final_action(F f) : clean(f) {}
		~final_action() { clean(); }

		F clean;
	};

	template <typename F> 
	final_action<F> finally(F f) {
		return final_action<F>(f);
	}

	enum class select_mode {
		 read
		,write
		,err
	};

	std::pair<std::string, std::string> sockaddr_to_pair(struct ::sockaddr_in addr, const int family = AF_INET);
}

#endif
//new comment
