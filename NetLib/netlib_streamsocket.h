#pragma once
#include "stdafx.h"

#ifndef NETLIB_STREAMSOCKET
#define NETLIB_STREAMSOCKET

namespace ntlb
{
	//����� ������� ����� ��������� ����� ����������� ��������� send � recv 
	//� ������ base_socket
	class socket_buf : public std::streambuf, public ntlb::base_socket
	{
	public:
		socket_buf() noexcept { init_(); }
		socket_buf(base_socket&& s) noexcept : base_socket(std::move(s)) { init_(); }
		socket_buf(const int socket_type, const int ip_proto, const int address_family) 
			: base_socket(socket_type, ip_proto, address_family) {	init_(); }

		socket_buf(const socket_buf&) = delete;
		socket_buf& operator= (const socket_buf&) = delete;

		socket_buf(socket_buf&&) = default;
		socket_buf& operator= (socket_buf&&) = default;

		~socket_buf() noexcept { this->flush_(); }

		//@brief size of the buffers
		static const size_t size_buf = BUFFER_SIZE_;
		//@brief number of characters in the buffer to cancel the reading
		static const size_t under_sym = 3;

		void close();

	protected:
		virtual int overflow(int c);

		virtual int underflow();

		virtual int sync(); 

		virtual std::streamsize xsputn(const char* s, std::streamsize n);

		virtual std::streamsize xsgetn(char* s, std::streamsize n);

	private:
		//@brief init pointer in the buffers  
		void init_() noexcept;

		//@brief send to the socket the symbols from the buffer
		int flush_() noexcept;

		//@brief receive the symbols from the socket to the buffer
		int fill_() noexcept;

		//@brief buffer from the sending
		char bout_[size_buf];
		//@brief buffer that will receive
		char bin_[size_buf];
	};
}

#endif 