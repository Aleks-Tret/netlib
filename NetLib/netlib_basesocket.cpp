#include "stdafx.h"
#include "netlib_basesocket.h"

ntlb::base_socket::base_socket(const int socket_type, const int ip_proto, const int address_family)
{
	this->open(socket_type, ip_proto, address_family);
}

ntlb::base_socket::base_socket(base_socket && that) noexcept : sd_(that.sd_)
{
	that.sd_ = INVALID_SOCKET;
}

ntlb::base_socket & ntlb::base_socket::operator=(base_socket && that)
{
	if (this == &that)
		return *this;

	if (sd_ != INVALID_SOCKET)
		this->close();

	std::swap(this->sd_, that.sd_);
	
	return *this;
}

ntlb::base_socket::~base_socket()
{
	try {
		close();
	}
	catch (ntlb::socket_error){
		return;
	}
}

void ntlb::base_socket::open(const int socket_type, const int ip_proto, const int address_family)
{
	sd_ = ::socket(address_family, socket_type, ip_proto);
	if (sd_ == INVALID_SOCKET)
		throw ntlb::socket_error(::WSAGetLastError(), "socket function fail");
}

void ntlb::base_socket::close()
{
	this->shutdown(ntlb::shut_mode::shut_rdwr);

	//const int size = 1024;
	//char* buffer = new char[size];
	//auto action = ntlb::finally([&buffer] {delete[] buffer; });

	//while (::recv(sd_, buffer, size, 0) != SOCKET_ERROR);

	//struct ::linger l {0};
	//this->set_option(SOL_SOCKET, SO_DONTLINGER, l);

	this->set_option(SOL_SOCKET, SO_DONTLINGER, 0);

	if (is_valid() && ::closesocket(sd_) == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "closesocket function fail");
	sd_ = INVALID_SOCKET;
}

void ntlb::base_socket::connect(const ntlb::net_addr & addr) const
{
	if (timeout_ != 0.0)
	{
		this->set_unblocking(true);

		int result = ::connect(sd_, addr.get_addrinfo()->ai_addr, static_cast<int>(addr.get_addrinfo()->ai_addrlen));
		if (result == SOCKET_ERROR)
		{
			if (!this->select(ntlb::select_mode::write))
				throw ntlb::timeout_error("function connect said timeout expired");
			//else -> socket is connected and need to block it
		}

		this->set_unblocking(false);
	}
	else
	{
		int result = ::connect(sd_, addr.get_addrinfo()->ai_addr, static_cast<int>(addr.get_addrinfo()->ai_addrlen));
		if (result == SOCKET_ERROR)
			throw ntlb::socket_error(WSAGetLastError(), "connect function failed");
	}	
}

void ntlb::base_socket::connect(const std::string & host, const std::string & port) const
{
	ntlb::net_addr addr(host, port, ntlb::net_addr::init_addrinfo());
	this->connect(addr);
}

void ntlb::base_socket::bind(const ntlb::net_addr & addr) const
{
	int result = ::bind(sd_, addr.get_addrinfo()->ai_addr, 
		addr.get_addrinfo()->ai_addrlen);
	if(result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "bind function fail");
}

void ntlb::base_socket::bind(const std::string & port, const std::string & host) const
{
	ntlb::net_addr addr(host, port);
	this->bind(addr);
}

void ntlb::base_socket::bind(int port, const std::string& host) const
{
	ntlb::net_addr addr(host, std::to_string(port));
	this->bind(addr);
}

void ntlb::base_socket::listen(int maxconn) const
{
	int result = ::listen(sd_, maxconn);
	if (result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "listen function fail");
}

ntlb::base_socket ntlb::base_socket::accept(struct ::sockaddr_in& remote)
{
	if (!this->select(ntlb::select_mode::read))
		throw ntlb::timeout_error("function accept said timeout expired");

	int len = sizeof(remote);

	SOCKET new_socket = ::accept(sd_, (struct ::sockaddr*)&remote, (int*)&len);
	if (new_socket == INVALID_SOCKET)
		throw ntlb::socket_error(WSAGetLastError(), "function accept fail");

	return ntlb::base_socket(new_socket);
}

void ntlb::base_socket::shutdown(ntlb::shut_mode how) const
{
	int result = ::shutdown(sd_, static_cast<int>(how));
	if (result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "shutdown function fail");	
}

void ntlb::base_socket::set_unblocking(bool is_unblock) const
{
	unsigned long on{};
	if (is_unblock)
		on = 1;
	
	int result = ::ioctlsocket(sd_, FIONBIO, &on);
	if (result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "ioctlsocket function fail");
}

int ntlb::base_socket::send(const char * message, size_t size, ntlb::msg_mode flag) const
{
	int rest{};

	while (size > 0) {
		if (timeout_ != 0.0)
			if (!this->select(ntlb::select_mode::write))
				throw ntlb::timeout_error("function send said timeout expired");

		int result = ::send(sd_, message + rest, size, static_cast<int>(flag));
		if (result == SOCKET_ERROR)
			throw ntlb::socket_error(WSAGetLastError(), "send function fail");

		size -= result;
		rest += result;
	}
	return rest;
}

int ntlb::base_socket::send(const std::string & message, ntlb::msg_mode flag) const
{
	return this->send(message.data(), message.size(), flag);
}

int ntlb::base_socket::sendto(const char * message, size_t size, ntlb::net_addr & addr, ntlb::msg_mode flag) const
{
	int rest{};
	while (size > 0) {
		if (timeout_ != 0.0)
			if (!this->select(ntlb::select_mode::write))
				throw ntlb::timeout_error("function sendto said timeout expired");

		int result = ::sendto(sd_, message + rest, size, static_cast<int>(flag),
			addr.get_addrinfo()->ai_addr, static_cast<int>(addr.get_addrinfo()->ai_addrlen));
		if (result == SOCKET_ERROR)
			throw ntlb::socket_error(WSAGetLastError(), "sendto function fail");

		size -= result;
		rest += result;
	}
	return rest;
}

int ntlb::base_socket::sendto(const std::string & message, ntlb::net_addr & addr, ntlb::msg_mode flag) const
{
	return this->sendto(message.data(), message.size(), addr, flag);
}

int ntlb::base_socket::recv(char * message, size_t size, ntlb::msg_mode flag) const
{
	if (timeout_ != 0.0)
		if (!this->select(ntlb::select_mode::write))
			throw ntlb::timeout_error("function recv said timeout expired");

	int result = ::recv(sd_, message, size, static_cast<int>(flag));
	if (result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "recv function fail");
	return result;
}

int ntlb::base_socket::recv(std::string & message, 
	size_t size, ntlb::msg_mode flag) const
{
	if (size > message.max_size())
		size = message.max_size();

	char* buffer = new char[size]; //I don't like it
	auto action = ntlb::finally([&buffer] {delete[] buffer; });

	int result = this->recv(buffer, size, flag);
	message.assign(buffer, result);

	return result;
}

int ntlb::base_socket::recvfrom(char * message, size_t size, std::string& addr, ntlb::msg_mode flag) const
{
	struct ::sockaddr_in remote {0};
	
	int result = this->recvfrom(message, size, remote, flag);

	addr = ntlb::sockaddr_to_pair(remote, AF_INET).first; //IPv4 address

	return result;
}

int ntlb::base_socket::recvfrom(std::string & message, size_t size, ::sockaddr_in remote, ntlb::msg_mode flag) const
{
	if (size > message.max_size())
		size = message.max_size();

	char* buffer = new char[size]; //I don't like it
	auto action = ntlb::finally([&buffer] {delete[] buffer; });

	int result = this->recvfrom(buffer, size, remote, flag);
	message.assign(buffer, result);

	return result;
}

int ntlb::base_socket::recvfrom(char * message, size_t size, struct ::sockaddr_in remote, ntlb::msg_mode flag) const
{
	if (timeout_ != 0.0)
		if (!this->select(ntlb::select_mode::write))
			throw ntlb::timeout_error("function recvfrom said timeout expired");

	size_t len = sizeof(remote);

	int result = ::recvfrom(sd_, message, size, static_cast<int>(flag),
		(struct ::sockaddr*)&remote, (int*)&len);
	if (result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "recvfrom function fail");

	return result;
}

bool ntlb::base_socket::select(ntlb::select_mode mode) const
{
	struct ::fd_set sct_fd { 1, { sd_ } };
	int result{};

	struct ::timeval tv { 0, 0 };
	if (timeout_ != 0.0) {
		tv.tv_sec = int(timeout_);
		tv.tv_usec = int((timeout_ - tv.tv_sec) * 1000000);
	}

	switch (mode)
	{
	case ntlb::select_mode::read:
		result = ::select(0, &sct_fd, nullptr, nullptr, &tv);
		break;
	case ntlb::select_mode::write:
		result = ::select(0, nullptr, &sct_fd, nullptr, &tv);
		break;
	case ntlb::select_mode::err:
		result = ::select(0, nullptr, nullptr, &sct_fd, &tv);
		break;
	default:
		break;
	}

	if (result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "select function fail");
	return result > 0 ? true : false;
}

struct ::sockaddr_in ntlb::base_socket::local_address() const
{
	struct ::sockaddr_in local_host { 0 };
	size_t len = sizeof(local_host);

	int result = ::getsockname(sd_, (sockaddr*)&local_host, (int*)&len);
	if (result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "getsockname function fail");

	return local_host;
}

::sockaddr_in ntlb::base_socket::remote_address() const
{
	struct ::sockaddr_in remote_host { 0 };
	size_t len = sizeof(remote_host);

	int result = ::getpeername(sd_, (sockaddr*)&remote_host, (int*)&len);
	if (result == SOCKET_ERROR)
		throw ntlb::socket_error(WSAGetLastError(), "getpeername function fail");

	return remote_host;
}
