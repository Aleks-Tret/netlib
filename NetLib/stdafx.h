// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������, ��
// �� ����� ����������
//

#pragma once
#pragma comment(lib, "ws2_32.lib")

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // ��������� ����� ������������ ���������� �� ���������� Windows


// TODO: ���������� ����� ������ �� �������������� ���������, ����������� ��� ���������
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <string>


#include "netlib_exception.h"
#include "netlib_def.h"
#include "netlib_netaddress.h"
#include "netlib_basesocket.h"
#include "netlib_streamsocket.h"