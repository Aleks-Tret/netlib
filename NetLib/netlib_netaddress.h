#pragma once
#include "stdafx.h"

#ifndef NETLIB_NETADDRESS
#define NETLIB_NETADDRESS

namespace ntlb
{
	class net_addr
	{
	public:
		net_addr(const std::string&, const std::string&, const struct ::addrinfo& hint);
		net_addr(const std::string&, const std::string&);
		net_addr(); //? maybe = delete?

		net_addr(net_addr&&) noexcept; 
		net_addr& operator=(net_addr&&) noexcept;

		net_addr(const net_addr&) = delete;
		net_addr& operator=(const net_addr&) = delete;

		~net_addr();

		//@brief creates a struct addrinfo with the specified options
		static struct ::addrinfo init_addrinfo(const int socktype = SOCK_STREAM, const int protocol = IPPROTO_TCP,
			const int family = AF_INET, const int flags = AI_PASSIVE) noexcept;

		//@brief return info about a host name from the network
		std::pair<std::string, std::string> get_nameinfo(const int flags = NULL) const;

		//@brief return const pointer to struct addrinfo
		const struct ::addrinfo* get_addrinfo() const noexcept { return address_; } //noexcept?

	private:
		struct ::addrinfo* address_ = nullptr;
	};
}

#endif