#pragma once
#include "stdafx.h"

#ifndef NETLIB_BASESOCKET
#define NETLIB_BASESOCKET

namespace ntlb
{
	class base_socket
	{
	public:
		base_socket() noexcept {}
		explicit base_socket(SOCKET socket) noexcept : sd_(socket) {} //will protected? noexcept?
		explicit base_socket(const int socket_type, const int ip_proto,
			const int address_family);

		base_socket(base_socket&&) noexcept;
		base_socket& operator=(base_socket&&);

		base_socket(const base_socket&) = delete;
		base_socket& operator=(const base_socket&) = delete;

		~base_socket();

		//@brief opens socket descriptor 
		void open(const int socket_type = SOCK_STREAM,
			const int ip_proto = IPPROTO_TCP,
			const int address_family = AF_INET);

		//@brief close socket descriptor
		void close();

		bool is_valid() const noexcept { return sd_ != INVALID_SOCKET; }
		
		operator bool() const noexcept { return sd_ != INVALID_SOCKET; }

		//@brief wrapper for ::setsockopt(), set the socket options
		template <typename T>
		void set_option(const int level, const int name, const T& v) const
		{
			T value{ v };
			int result = ::setsockopt(sd_, level, name, (char*)&value, sizeof(T));
			if (result == SOCKET_ERROR)
				throw ntlb::socket_error(WSAGetLastError(), "setsockopt function fail");
		}

		//@brief wrapper for ::getsockopt(), get the socket options
		template <typename T>
		T get_option(const int level, const int name) const
		{
			T value{ 0 };
			int size_T = sizeof(T);
			int result = ::getsockopt(sd_, level, name, (char*)&value, &size_T);
			if (result == SOCKET_ERROR)
				throw socket_error(WSAGetLastError(), "getsockopt function fail");
			return value;
		}

		//@brief connects socket to remote address
		void connect(const ntlb::net_addr&) const;
		void connect(const std::string&, const std::string&) const;

		//@brief binds socket to local address
		void bind(const ntlb::net_addr&) const;
		void bind(const std::string& port, const std::string& host = std::string()) const;
		void bind(int port, const std::string& host = std::string()) const;

		//@brief puts socket in listening mode
		void listen(int maxconn = SOMAXCONN) const;

		//@brief accepts connection request
		//returns the connected socket
		ntlb::base_socket accept(struct ::sockaddr_in& remote);

		//@brief returns socket descriptor
		//����� ��� �����?!
		int get() const noexcept { return sd_; }
		operator int() const noexcept { return sd_; }

		//@brief closes all or part of a connection on a socket
		void shutdown(ntlb::shut_mode) const;

		//@brief unsets/sets bloking mode for the socket
		void set_unblocking(bool) const;

		//@brief sends data on a connected socket
		int send(const char* message, size_t size, 
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;

		int send(const std::string& message, 
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;

		//@brief sends data from another socket
		int sendto(const char* message, size_t size, ntlb::net_addr& addr,
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;

		int sendto(const std::string& message, ntlb::net_addr& addr,
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;

		//@brief receives data from a connected socket or a bound connectionless socket
		int recv(char* message, size_t size,
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;

		int recv(std::string& message, size_t size,
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;

		//@brief reads data from another socket
		int recvfrom(char* message, size_t size, struct ::sockaddr_in remote,
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;

		//@brief gives address ipv4
		int recvfrom(char* message, size_t size, std::string& remote,
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;

		int recvfrom(std::string& message, size_t size, struct ::sockaddr_in remote,
			ntlb::msg_mode flag = ntlb::msg_mode::msg_non) const;
		
		//@brief determines the status of the socket
		bool select(ntlb::select_mode mode) const;

		//@brief sets a timeout on the following operations.
		//if time is 0 timeout is cancelled
		void set_timeout(double time) { timeout_ = time; }
		double get_timeout() { return timeout_; }

		//@brief returns local address to which socket is bound
		struct ::sockaddr_in local_address() const;

		//@brief returns remote address to which socket is connected
		struct ::sockaddr_in remote_address() const;

	private:
		SOCKET sd_ = INVALID_SOCKET;
		double timeout_{};
	};
}

#endif